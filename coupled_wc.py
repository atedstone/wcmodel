"""
This is a clumsy attempt at coupling:

1. ebmodel
2. wcmodel
3. algae_pop_model

In an online fashion.

May 2020, Fribourg.

Known issues:
- rho spinup is suspect
- use of MAR 'snowpack' temperatures is problematic as only 1 value output per day, so no diurnal cycling
	- also not sure how MAR spins up snowpack itself but perhaps this doesn't matter too much

"""

import numpy as np
import pandas as pd
import xarray as xr
import datetime as dt
import pyproj
import matplotlib.pyplot as plt
import math

import imauaws
import promice
import mar_raster

import ebmodel as ebm

import algae_pop_model as apm

import wcmodel



### PARAMETERS ###############################################################

plot_results = True

## Site location

# site_type = 'IMAU'
# site_id = 'S6'
# px_lon = -49.35890
# px_lat = 67.07454
# lon_ref = -45.
# slope = 1.
# aspect = 90.
# elevation = 1000.
# roughness = 0.002 # c.f. Gribbin ref for rough ice
# slopSV = 0.013076

site_type = 'MAR'
site_id = 'S6'
px_lon = -49.35890
px_lat = 67.07454
lon_ref = -45.
slope = 1.
aspect = 90.
elevation = 1000.
roughness = 0.002 # c.f. Gribbin ref for rough ice
slopSV = 0.013076
initial = 179.457

# site_type = 'IMAU'
# site_id = 'S5'
# px_lon = -50.08573
# px_lat = 67.097883
# lon_ref = -45.
# slope = 1.
# aspect = 90.
# elevation = 500.
# roughness = 0.002 # c.f. Gribbin ref for rough ice
# slopSV = 0.0179

# site_type = 'MAR'
# site_id = 'S5'
# px_lon = -50.08573
# px_lat = 67.097883
# lon_ref = -45.
# slope = 1.
# aspect = 90.
# elevation = 500.
# roughness = 0.002 # c.f. Gribbin ref for rough ice
# slopSV = 0.0179

# site_type = 'IMAU'
# site_id = 'S9'
# px_lon = -48.251917
# px_lat = 67.05185
# lon_ref = -45.
# slope = 1.
# aspect = 90.
# elevation = 1500.
# roughness = 0.000126 # Kuipers-Munneke 2018 AAAR
# slopSV = 0.006745

# site_type = 'MAR'
# site_id = 'S9'
# px_lon = -48.251917
# px_lat = 67.05185
# lon_ref = -45.
# slope = 1.
# aspect = 90.
# elevation = 1500.
# roughness = 0.000126 # Kuipers-Munneke 2018 AAAR
# slopSV = 0.006745

# S10 not available from Spring 2016 onwards

# site_type = 'PROMICE'
# site_id = 'KAN_U'
# px_lon = -47.017
# px_lat = 67.
# lon_ref = -45.
# slope = 1.
# aspect = 90.
# elevation = 1840.
# roughness = 0.002 # c.f. Gribbin ref for rough ice

# site_type = 'PROMICE'
# site_id = 'KAN_M'
# px_lon = -48.8355
# px_lat = 67.0670
# lon_ref = -45.
# slope = 1.
# aspect = 90.
# elevation = 1270.
# roughness = 0.002 # c.f. Gribbin ref for rough ice
# slopSV = 0.

## Year
year = 2016
mar_q = '57' # the 'q' value at the end of ICE filenames. #2012=35, 13=36, 14=37, 15=38, 16=39
prev_day = '2016-06-13'
ts_start = '2016-06-13 12:00' #'2016-06-01 12:00' 
ts_end = '2016-08-31'


albout_bb_cld = 0.8




## Algal model parameters
# NOte that this is variable in current MAR implementation !!
pop_size = 0. + initial
#density_scale = 0.33 
perc_loss = 0.11
SnHmax = 0.1
mltmin = 1.5
SWdmin = 50.

# MODIS data at 15 km resolution.
alb = xr.open_dataset('/scratch/MOD10A1.006.MAR_GRo_trim/MOD10A1.2016.006.MAR_GRo_trim.nc')
# Set grid coordinates to exactly match MAR's
mar_15 = mar_raster.open_xr('/scratch/MAR_GRo_out/ICE.2016.06-08.o01.nc')
alb['Y'] = mar_15.Y  
alb['X'] = mar_15.X  
alb_proj = mar_raster.create_proj4(ds_fn='/scratch/MAR_GRo_out/ICE.2016.06-08.o01.nc')


# MODIS data at their 'native' resolution to compare this model run against.
albn = xr.open_dataset('/scratch/MOD10A1.006.SW/MOD10A1.%s.006.reproj.nc' %year)
albn_proj = pyproj.Proj("+proj=stere +lat_0=70.5 +lon_0=-40 +k=1 +datum=WGS84 +units=m")


# We always load MAR data, regardless of site type. This is so we can compare with new model later.
mar_ds = mar_raster.open_xr('/scratch/MARv3.9-20km-ERAI/ICE.2016.01-12.c56.nc')
mar = mar_ds.sel(TIME=slice('%s-06-01' %year,'%s-09-01' %year))
mar_proj = mar_raster.create_proj4(ds=mar)
cx,cy = mar_proj(px_lon,px_lat) 
snow_above_ice = mar.SHSN2.sel(X=cx, Y=cy, method='nearest').sel(SECTOR1_1=1.0).squeeze().to_series().resample('1D').first()
clouds = mar.CC.sel(X=cx, Y=cy, method='nearest').to_series().resample('1D').first()
surface_rho = mar.RO1.sel(X=cx, Y=cy, method='nearest').isel(OUTLAY=0).to_series().resample('1D').first()
rainfall = mar.RF.sel(X=cx, Y=cy, method='nearest').to_series().resample('1D').first()
sws_ts = mar.SWSN.sel(X=cx,Y=cy, method='nearest').squeeze().to_series().resample('1D').first()




## Load met. station data
### Lots of different options here...

if site_type == 'IMAU':

	def date_parser(year, day, hour):
		year = int(float(year))
		day = int(float(day))
		hour = int(float(hour))
		return dt.datetime.strptime('%i-%i-%i' %(year,day,hour), '%Y-%j-%H')

	if year in np.arange(2012, 2017) and site_id == 'S6':
		fn = '/scratch/metdata/S6_SEB_2012_2016_Tedstone.txt'
	elif year in np.arange(2010, 2012) and site_id == 'S6':
		fn = '/scratch/metdata/S6_SEB_2010_2011_Tedstone.txt'
	elif year in np.arange(2008, 2011) and site_id == 'S6':
		fn = '/scratch/metdata/S6_SEB_2008_2010_Tedstone.txt'
	elif year in np.arange(2003, 2008) and site_id == 'S6':
		fn = '/scratch/metdata/S6_SEB_2003_2007_Tedstone.txt'

	elif site_id == 'S5':
		fn = '/scratch/metdata/S5_SEB_2003_2016_Tedstone.txt'
	elif site_id == 'S9':
		fn = '/scratch/metdata/S9_SEB_2003_2016_Tedstone.txt'
	elif site_id == 'S10':
		fn = '/scratch/metdata/S10_SEB_2003_2016_Tedstone.txt'


	met_data_full = pd.read_csv(fn,
		delim_whitespace=True, parse_dates={'datetime':[0,1,2]}, 
		date_parser=date_parser, index_col='datetime', 
		keep_date_col=True)

	met_data = met_data_full[ts_start:ts_end]

	met_data = met_data.drop(labels=('time'), axis=1)
	met_data = met_data.filter(items=('SWin_corr', 'pres', 'Tair_2m', 'qair_2m', 'hour'))

	# Convert hour to time format needed by ebm
	time = met_data.hour.astype('float') * 100
	time[time == 0] = 2400
	met_data['hour'] = time

	# Met data needs columns of: airtemp, windspeed, avp, time, swd
	met_data = met_data.rename({'SWin_corr':'swd', 'pres':'avp', 
		'Tair_2m':'airtemp', 'qair_2m':'windspeed', 'hour':'time'}, axis='columns')


if site_type == 'PROMICE':

	fn = '~/Dropbox/work/data/promice_data_20171020-102527/'
	fn = fn + site_id + '_hour.txt'
	met_data_full = promice.load(fn, freq='hourly')
	met_data = met_data_full[ts_start:ts_end]
	met_data = met_data.filter(items=('AirTemperature(C)', 'AirPressure(hPa)',
		'ShortwaveRadiationDown_Cor(W/m2)', 'WindSpeed(m/s)'))
	met_data['hour'] = met_data.index.hour.astype('float') * 100
	met_data['hour'][met_data['hour'] == 0] = 2400
	met_data = met_data.rename({'ShortwaveRadiationDown_Cor(W/m2)':'swd', 'AirPressure(hPa)':'avp', 
		'AirTemperature(C)':'airtemp', 'WindSpeed(m/s)':'windspeed', 'hour':'time'}, axis='columns')

if site_type == 'MAR':
	mar_air_temp = mar.TTH.sel(X=cx, Y=cy, method='nearest').to_pandas().stack()
	mar_swd = mar.SWDH.sel(X=cx, Y=cy, method='nearest').to_pandas().stack()
	mar_pres = mar.SPH.sel(X=cx, Y=cy, method='nearest').to_pandas().stack()
	mar_wind_x = mar.UUH.sel(X=cx, Y=cy, method='nearest').to_pandas().stack()
	mar_wind_y = mar.VVH.sel(X=cx, Y=cy, method='nearest').to_pandas().stack()
	mar_wind = (mar_wind_x **2 + mar_wind_x**2) ** 0.5
	mar_index = pd.date_range(start=mar_air_temp.index[0][0].strftime('%Y-%m-%d') + ' 01:00', end='2016-09-02', freq='1H')
	met_data = pd.DataFrame({'airtemp':mar_air_temp, 
		'swd':mar_swd,
		'avp':mar_pres,
		'windspeed':mar_wind})
	met_data.index = mar_index
	met_data['time'] = met_data.index.hour.astype('float') * 100
	met_data['time'][met_data['time'] == 0] = 2400
	met_data = met_data[:'2016-08-31']


## SOLAR ZENITH ANGLE
# https://www.esrl.noaa.gov/gmd/grad/antuv/SolarCalc.jsp
import datetime as dt
def dateparse(year,month,day,hour,minute,second):
	return dt.datetime(int(year),int(month),int(day),int(hour),int(minute),int(second))

zen = pd.read_csv('/home/at15963/Dropbox/work/papers/tedstone_algaemelt/model1D/solar_zenith_s6_esrl_noaa.txt',
	delim_whitespace=True, parse_dates={'ts':[0,1,2,3,4,5]},index_col='ts',date_parser=dateparse )
zen[zen > 89] = 89
zen = zen / 360. * 2 * math.pi   

## Set up remaining EB_AUTO parameters
lat = px_lat
lon = px_lon
summertime = 1
met_elevation = elevation
lapse = 0.


# END USER-SELECTABLE PARAMETERS #############################################

ts = 60 * 60 # hourly timestep

# Set up WC model
#layers_thk = np.array([0.02, 0.02, 0.04, 0.04, 0.16, 0.16, 0.16, 0.16, 0.32, 0.32])
# Spun up values
#model.rho = np.array([299., 299., 301., 409., 593., 777., 847., 874., 887.])

layers_thk = np.array([0.2, 0.2, 0.2, 0.4, 0.8, 0.8])
rho_spunup = np.array([300,387,684,814,862, 890])

# MAR snow layering
# Full integration with MAR snow layers would indeed be more elegant memory-wise, but perhaps not my problem now
layers_thk = np.array([0.05, 0.05, 0.1, 0.1, 0.1, 0.1, 0.15, 0.15, 0.2])
rho_spunup = np.array([920]*len(layers_thk))

# Timestep, seconds
delta_t = ts
# Corresponding density of ice kg m-3 for lhfi
rhoI = 920. 
# Initial density at t0, kg m-3.
rhoMax = rhoI 
# Minimum density (once a layer is at rhoMin then K_star energy is used for surface lowering instead)
rhoMin = 450.
model = wcmodel.wcmodel(layers_thk, delta_t, rhoMax, rhoMin)
# Spun up values
model.rho = rho_spunup


## Set up stores for calculated values
store_spec = {}
store_params = []
flux_store = {}
melt_store = {}


mar_at_pt = mar.sel(X=cx, Y=cy, method='nearest')
melt_run_avg = mar_at_pt.ME.to_pandas().squeeze().rolling('21D').mean()
air_temp = met_data.airtemp.rolling('21D').mean()

melt_hrs = 0
prod_hrs = 0

for ts, met in met_data.iterrows():

	czenang = np.min([0.99, np.max([0.02, np.min([0.5, np.cos(zen.iloc[zen.index.get_loc(ts, method='nearest')]) ]) ]) ])

	#print(ts)
	
	# ------------------------------------------------------------------------
	### CALCULATE ALGAL POPULATION

	# Allow algal growth to occur on bare ice and under a thin snow covering
	# (if other climate pre-requisites also met)
	if snow_above_ice[snow_above_ice.index.get_loc(ts, method='ffill')] < SnHmax:
		# Update size of algal population
		# Need to allow growth to continue to occur under thin snow
		if met.time == 1200:
			prev_day = (ts-pd.Timedelta(days=1)).strftime('%Y-%m-%d')
			today = met_data[prev_day].resample('1H').first()
			
			# Hours in day with SWin above threshold (PAR)
			h_par = (today.swd.resample('1H').mean() > SWdmin).sum()
			prod_hrs = np.min([h_par, melt_hrs])
			pop_size = apm.model2_pop(pop_size, prod_hrs / 24, perc_loss)
			pop_size = np.max([initial, pop_size])
			melt_hrs = 0
			print(ts)


	# ------------------------------------------------------------------------
	### CALCULATE ENERGY BALANCE

	# Ideally need different roughness values here??
	SWR,LWR,SHF,LHF = ebm.calculate_seb(
		lat, lon, lon_ref, int(ts.strftime('%j')), int(met.time), summertime,
		slope, aspect, elevation, met_elevation, lapse,
		met.swd, met.avp, met.airtemp, met.windspeed, albout_bb_cld, roughness)

	SWR = np.nan_to_num(SWR)
	LWR = np.nan_to_num(LWR)
	SHF = np.nan_to_num(SHF)
	LHF = np.nan_to_num(LHF)

	# Calculate melting
	swmelt,lwmelt,shfmelt,lhfmelt,totalmelt = ebm.calculate_melt(SWR, LWR, SHF, LHF,
		met.windspeed, met.airtemp)
	
	if totalmelt >= mltmin: #1.7: #1.5: #1.5 is a bit closer to MAR 1mm equivalent, as EB AUTO runs too warm in comparison
	#if met.airtemp > 0.5:
		melt_hrs += 1


	# ------------------------------------------------------------------------
	### COMMENCE ALBEDO CALCULATIONS -->

	## Only calculate albedo if we are dealing with a bare ice surface.
	if surface_rho.iloc[surface_rho.index.get_loc(ts, method='ffill')] >= 917.: #snow == 0 seems very unreliable

		popsize_slope = pop_size * (-60. * slopSV + 1.71)

		# Weathering crust
		a = np.array([2.]*len(layers_thk))
		a[0] = 0.0002 * popsize_slope + 2. #pop0:a=2, pop10000:a=4
		layer_temps = mar_at_pt.TI1.sel(TIME=ts.date().isoformat()).isel(OUTLAY=slice(0,len(a))).values.flatten()
		rho, M, Mi, Ma, Mc, T_ice, Q_Mi = model.run(SWR, LWR, SHF, LHF, a, layer_temps)
		rhoavg = model.calc_depth_avg_rho()
		albout_bb = np.maximum(0.4, np.minimum(0.6, -0.002 * rhoavg + 1.8))

		# Add on effect of algae
		albout_bb = albout_bb + (-2e-5 * popsize_slope)
		albout_bb = np.maximum(0.2, albout_bb)


		# Cloud correct as per MAR
		czemax = 0.173648178
		csegal = np.max([czemax,czenang])
		dalbeW =((0.64 - csegal  )*0.0625) 
		dalbed = dalbeW 
		cldts = clouds[ts.strftime('%Y-%m-%d')]
		albout_bb_cld = albout_bb + 0.05 * (cldts-0.5) + dalbed * (1.-cldts)
		
		# Force minimum albedo
		albout_bb_cld = np.max([0.2, albout_bb_cld])
		#albout_bb = np.max([0.2, albout_bb])

		## Outputs
		store_here = dict(albclr=albout_bb, albcld=albout_bb_cld, 
			algae_mgg=popsize_slope,  algae_pop=pop_size,
			SWR_Wm2=SWR, LWR_Wm2=LWR, SHF_Wm2=SHF, LHF_Wm2=LHF,
			SWR_melt=swmelt, LWR_melt=lwmelt, SHF_melt=shfmelt, LHF_melt=lhfmelt,
			totalmelt=totalmelt,
			solaz=czenang, prod_hrs=prod_hrs, date=ts,
			rhoavg=rhoavg)

		
	## Albedo not run - fill vars
	else:
		store_here = dict(albclr=np.nan, albcld=np.nan,  
			algae_mgg=np.nan, flg_atm=np.nan, 
			algae_pop=pop_size, 
			SWR_Wm2=SWR, LWR_Wm2=LWR, SHF_Wm2=SHF, LHF_Wm2=LHF,
			SWR_melt=swmelt, LWR_melt=lwmelt, SHF_melt=shfmelt, LHF_melt=lhfmelt,
			totalmelt=totalmelt, prod_hrs=prod_hrs,
			solaz=np.nan, date=ts)

	store_params.append(store_here)



# Parameters used for every timestep
daily_params = pd.DataFrame(store_params)
daily_params.set_index('date', inplace=True, drop=True)
# Daily albedo time series
alb_ts_cld = daily_params.albcld.where(met_data.swd > 250).resample('1D').mean()
alb_ts_clr = daily_params.albclr.where(met_data.swd > 250).resample('1D').mean()
# Energy balance fluxes
fluxes = pd.DataFrame.from_dict(flux_store, orient='index')
# Energy balance melt rates
melt_rates = pd.DataFrame.from_dict(melt_store, orient='index')
clr_alb_terra = daily_params.albclr.iloc[daily_params.index.indexer_between_time('12:00:00', '16:00:00')].resample('1D').mean()


# MODIS at MAR resolution
ax,ay = alb_proj(px_lon,px_lat) 
modis_alb_ts = alb.Snow_Albedo_Daily_Tile.sel(X=ax, Y=ay, method='nearest').to_series() / 100
modis_alb_ts[modis_alb_ts > 1] = np.nan
# 7-Day running average
modis_alb_ts_sm = modis_alb_ts.rolling('7D').mean()

# MODIS albedo at ~native resolution
alnx,alny = albn_proj(px_lon,px_lat)
modis_alb_ts_n = albn.Snow_Albedo_Daily_Tile.sel(X=alnx, Y=alny, method='nearest').to_series() / 100
modis_alb_ts_n[modis_alb_ts_n > 1] = np.nan

# MAR-predicted albedo - ought to remove cloud correction first
mar_alb_ts = mar.AL.sel(X=cx,Y=cy, method='nearest').squeeze().to_series().resample('1D').first()
mar_alb_ts_clear = mar_alb_ts - 0.05 * (clouds - 0.5) - 0.025

# Save to common data frame
combo = pd.concat((mar_alb_ts, alb_ts_clr, modis_alb_ts, modis_alb_ts_n, modis_alb_ts_sm, alb_ts_cld,clr_alb_terra),axis=1)
combo.columns = ['MAR', 'Here', 'MODIS', 'MODIS_native','MODIS 7D','Here_cld','Here_clr_terra']

if plot_results:
	plt.figure()
	combo['MAR'].plot(label='MAR_CloudFrac')
	combo['MODIS_native'].plot(marker='o', color='#969696', linestyle='none', markersize=3, label='MODIS 500 m')
	combo['MODIS'].plot(marker='o', color='#252525', linestyle='none', markersize=3, label='MODIS 15 km')
	combo['MODIS 7D'].plot(color='#252525', label='MODIS 15 km 7D')
	combo['Here'].plot(color='#08519C', label='Here_ClearSky')
	combo['Here_cld'].plot(color='gray', label='Here_CloudFrac')
	combo['Here_clr_terra'].plot(color='purple', label='Here_ClearSky_TerraOverPass')
	plt.legend()
	plt.ylim(0,1)
	plt.ylabel('Albedo')
	plt.xlim('%s-06-01' %year,'%s-09-01' %year)
	plt.title(site_id)
	plt.tight_layout()
	plt.subplots_adjust(bottom=0.2)
	#text = 'Growth: %s  Loss: %s  Z0: %s  dz_max: %s' %(prod_24h, loss_24h, roughness, thk_max)
	#text = 'Model: 3'
	#plt.text('%s-07-15' %year, -0.23, text, horizontalalignment='center')
	plt.show()
	#out_fn = 'albedo_%s_%s_%s_%s_%s.png' %(year,site_id,prod_24h,death_24h,thk_max)
	#out_fn = 'albedo_%s_%s_model_3.png' %(year,site_id)
	#plt.savefig('/home/at15963/Dropbox/work/papers/tedstone_algaemelt/model1D/'+out_fn,
#		dpi=300)
# Also plot native timestep albedo as well...
# daily_params.alb.plot()

