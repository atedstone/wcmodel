"""
Documentation to go here

surface area 1m2

"""

import numpy as np
import math
from copy import deepcopy


class wcmodel:

	# Latent heat of fusion, J/kg - J energy to melt 1 kg ice @ rhoI
	lhfi = 334000 
	# Snow heat capacity, J kg-1 K-1
	Cn = 2105.

	rho_store = {}
	M_store = {}

	rho = None
	M = None
	T_ice = None



	def __init__(self, layers_thk, ts, rhoMax, rhoMin, verbose=False):

		self.layers_thk = layers_thk
		self.ts = ts
		self.rhoMin = rhoMin
		self.rhoMax = rhoMax

		self.verbose = verbose

		# Set initial densities as the same for each layer
		self.rho = np.array([self.rhoMax]*self.nlyrs)

		# Calculate initial mass of ice, kg
		self.M = deepcopy(self.V_o) * deepcopy(self.rho)

		self.T_ice = -1.
		
	

	@property
	def nlyrs(self):
		return len(self.layers_thk)



	@property
	def zl(self):
		# Lower boundaries of each layer, metres
		return(np.cumsum(self.layers_thk))



	@property
	def zu(self):
		# Upper boundaries of each layer, metres
		return(self.zl - self.layers_thk)



	@property
	def V_o(self):
		"""
		Layer volumes (prescribed) m3
		depth (m) * width (m) * length (m)
		"""
		return self.layers_thk * 1 * 1


	
	def printv(self, *args):
		if self.verbose:
			print(*args)



	def calc_Q_Mi(self, K_star, a):
		Q_Mi = np.zeros(self.nlyrs)
		if np.ndim(a) < 1:
			a = np.array([a]*self.nlyrs)

		for n in np.arange(0, self.nlyrs):
			K_star_new = (K_star * math.e**(-a[n] * self.layers_thk[n]))
			Q_Mi[n] = (K_star - K_star_new) * self.ts
			K_star = K_star_new
			#print(K_star, Q_Mi[n])
		return Q_Mi




	def run(self, K_star, L_star, SHF, LHF, a, layer_temps):
		"""
		To do - check for integrity of each input DataType

		:returns: tuple (rho, mass)

		"""

		if layer_temps is not None:
			temp_mode = 'MAR'
		else:
			temp_mode = 'default'

		self.M = self.rho * self.V_o

		## Mass of ice lost through surface lowering, kg
		M_a = np.zeros(self.nlyrs)

		rho_old = deepcopy(self.rho)

		## 4.2 Energy input at each layer
		# the coefficient is NOT dependent on density so output of this eqn depends only on K_star.
		# This means that M will only change in response to energy inputs, not to internal propertiesq
		K_star = np.maximum(0, float(K_star))
		Q_Mi = self.calc_Q_Mi(K_star, a)

		self.printv('Q_Mi', Q_Mi)

		## 4.3 Energy inputs for surface lowering
		# L_star + Q_H + Q_E
		Q_Ma = (float(L_star) + float(SHF) + float(LHF)) * self.ts

		if temp_mode == 'default':
			eb = (K_star + L_star + SHF + LHF) * self.ts
			J = self.Cn * (np.sum(self.rho*self.get_lyr_proportions()) * (np.sum(self.layers_thk)*1*1))
			T_change = eb / J
			self.T_ice = self.T_ice + T_change
			self.T_ice = np.minimum(self.T_ice, 0)
			T_ice_flg = np.where(self.T_ice == 0, 1, 0)
			depth_flags = np.ones(self.nlyrs)
		elif temp_mode == 'MAR':
			if layer_temps[0] > -0.2:
				T_ice_flg = 1
			else:
				T_ice_flg = 0

			depth_flags = np.where(layer_temps > -0.2, 1, 0)
			T_change = np.nan
		else:
			raise ValueError('Unknown temperature mode')

#		T_ice_flg = 1

		## 4.4 Mass of melt at each level
		# kg
		#Lfh = np.array([self.lhfi]*self.nlyrs)
		M_i = (1 / self.lhfi) * Q_Mi * depth_flags
		# or:
		#M_i = Q_Mi / (self.rho * self.lhfi)

		## Energy conservation at uppermost surface level:
		# If Q_Ma is -ve, account for energy loss from the surface while 
		# allowing evolution of lower layers
		# Schuster pg 98
		#if Q_Ma < 0:
			# M_i[0] = np.maximum(0, (1 / self.lhfi) * (Q_Mi[0] + Q_Ma) )
			# or:
			#M_i[0] = (Q_Mi[0] + Q_Ma) / (self.rho[0] * self.lhfi)
		M_i[0] = M_i[0] * T_ice_flg
		
		## Calculate layer mass loss by collapse (some fraction of the layer)
		# This is not part of the original Schuster scheme.
		M_c = np.where(rho_old <= self.rhoMin, M_i, 0)
		## Calculate layer mass loss through density reduction
		M_i = np.where(rho_old > self.rhoMin,  M_i, 0)

		self.printv('M_i', M_i)

		## 4.5 Surface lowering in layer 1 (mass of melt - kg)
		M_a = np.maximum(0, (1 / self.lhfi) * Q_Ma ) * T_ice_flg
		# or (Schuster):
		#M_a = Q_Ma / (self.rho[0] * self.lhfi)
		self.printv('M_a', M_a)

		## 4.7 volume of ice lost through surface lowering (layer 1)
		# Required as input to Eqn. 4.6.a
		# m^3
		V_a = M_a / rho_old[0] # 'wherein rho is ice density at the beginning 
							   # of the time period'
		self.printv('V_a', V_a)
		
		## Volume of ice lost through layer collapse (all layers)
		V_c = M_c / rho_old

		## 4.6.a Compute new value of rho for uppermost layer
		# Here, we need to include both M_a and M_c[0]. This is because both 
		# M_a and M_c[0] could be occurring at the same time, whereas in lower
		# layers M_c only occurs when rho==rhoMin.
		# I THINK?? CHECK LOGIC OF M_C again....
		# kg m-3
		self.rho[0] = (self.M[0] - M_i[0] - M_a - M_c[0]) /                     \
					  (self.V_o[0] - V_a - V_c[0]) 
		
		## 4.6.b Compute new value of rho for L=2-6
		# V_c is only > 0 if rho[n] == rhoMin, at which point rho[n] is no 
		# longer allowed to change, so we don't include the V_c term in this
		# calculation.  NOPEEEEE
		# kg m-3
		self.rho[1:] = (self.M[1:] - M_i[1:] - M_c[1:]) / (self.V_o[1:] - V_c[1:])
		self.printv('rho', self.rho)
		
		## Volume remaining in each layer after melting during this time step
		# This will be replenished below ...
		# m^3
		V = self.V_o - V_c - V_a
		self.printv('V', V)

		## Volume lost from each layer at this point in time
		V_loss = np.cumsum(V_c) + V_a

		## 4.8 / 4.9 / 4.10 Replenishment of each layer volume 
		# Required as layer thicknesses are invariant, so each layer is topped
		# back up by mass from the layer below it.
		for n in np.arange(0, self.nlyrs): 
		
			# If at bottom layer, replenish using ice of rhoMax
			if n == self.nlyrs-1:
				rho_under = self.rhoMax
			# Otherwise replenish from layer n+1
			else:
				rho_under = self.rho[n+1]

			if V_loss[n] > 0:
				## 4.8 Replenishment layer by layer
				# Modified by AJT to occur gradually via V_loss term
				# m^3
				self.M[n] = (self.rho[n] * (self.V_o[n] - V_loss[n])) +         \
					(rho_under * V_loss[n])
			else:
				## Eqn 4.10 has limiting case when M_a and therefore V_a is 
				# equal to zero.
				# Internal melt is occurring in all layers but there is no 
				# replenishment allowed for in the simulation unless ablation
				# is occurring at the same time, thus:
				# Eqn 4.11
				# kg
				self.M[n] = self.rho[n] * V[n] 

			## Eqn 4.9, simplified as Eqn 4.10
			# Now that layers have been adjusted, calculate input density for 
			# the next time period
			# kg m-3
			self.rho[n] = (self.rho[n] * (1 - (V_loss[n] / self.V_o[n]))) +     \
						  (rho_under * (V_loss[n] / self.V_o[n]))
		
		# print('Q_Mi', Q_Mi)
		# print('Q_Ma', Q_Ma)
		# print('M_i', M_i)
		# print('M_c', M_c)
		# print('M_a', M_a)
		# print('V_loss', V_loss)
		# print('Rho old', rho_old)
		# print('Rho new', self.rho)
		# print('AVG', np.sum(self.get_lyr_proportions() * self.rho))
		# print(' ')

		#return (self.rho, self.M, np.sum(M_i), M_a, M_c, self.T_ice, Q_Mi)
		return (self.rho, self.M, np.sum(M_i), M_a, M_c, T_change, Q_Mi)



	def get_lyr_proportions(self):
		""" Proportion that each layer contains of overall column thickness.

		:returns: np.array

		"""
		return 1 / np.sum(self.layers_thk) * self.layers_thk



	def calc_depth_avg_rho(self, rho=None):
		""" Calculate depth-averaged value of rho, accounting for layer thickness.

		If rho not provided, returns value for rho array of last timestep.

		"""
		
		if rho is None:
			rho_here = self.rho
		else:
			rho_here = rho

		return (rho_here * self.get_lyr_proportions()).sum()




