import numpy as np
import math
import pandas as pd
from copy import deepcopy

## Six ice depths
# surface area 1m2

# Layer thicknesses
layers_thk = np.array([1, 2, 4, 8, 16, 32]) # cm
layers_thk = layers_thk / 100 # Convert to metres

# Layer volumes (prescribed) m3
# depth (m) * width (m) * length (m)
V_o = layers_thk * 1 * 1
V_a = np.zeros(6)
V = deepcopy(V_o)

delta_t = 300

# Decay term in Beer's Law, after Geiger 1965, their units are cm
a = 0.06 
a = a * 100 # convert to metres

# Lower boundaries of each layer
zl = np.cumsum(layers_thk)
# Upper boundaries of each layer
zu = zl - layers_thk

# Initial density is same for each layer
rho = np.array([890.]*6)

# Latent heat of fusion
lhfi = 334000 # J energy to melt 1 kg ice @ rhoI
rhoI = 920. # density of ice kg m-3

# Calculate mass of ice
M = deepcopy(V_o) * deepcopy(rho)
# Mass of ice lost through surface lowering
M_a = np.zeros(6)

# Net shortwave radiation
K_star = 351

times = np.arange(0,(60/5)*(24*1),1)
rho_store = {}
M_store = {}

verbose = False
def printv(*args):
	if verbose:
		print(*args)

for t in times:

	printv('\n\nTIME:', t)

	rho_old = deepcopy(rho)

	a = -0.033 * rho + 36.7

	# 4.2 Energy input at each layer
	# the coefficient is NOT dependent on density so output of this eqn depends only on K_star.
	# This means that M will only change in response to energy inputs, not to internal properties
	pt1 = (K_star * math.e**(-a * zu))
	pt2 = (K_star * math.e**(-a * zl))
	Q_Mi = (pt1 - pt2) * delta_t
	printv('Q_Mi', Q_Mi)

	# 4.3 Energy inputs for surface lowering
	# L_star * Q_H * Q_E
	Q_Ma = (-11 + 110 + -18) 

	# 4.4 Mass of melt at each level
	# kg
	#M_i = (Q_Mi / ((rho_old/917.) * lhfi)) 
	# First come up with some scaled approximation of latent heat of fusion appropriate to density at this time
	#Lfh = lhfi / rhoI * rho_old
	Lfh = np.array([lhfi]*6)
	M_i = (1 / Lfh) * Q_Mi
	printv('M_i', M_i)

	# If Q_Ma is -ve, account for energy loss from the surface while allowing evolution of lower layers
	# Schuster pg 98
	if Q_Ma < 0:
		M_i[0] = (1 / Lfh[0]) * (Q_Mi[0] + (Q_Ma * delta_t))
	printv('M_i', M_i)

	# 4.5 Surface lowering in layer 1 (mass of melt - kg)
	# (I added delta_t, not present in original model equations - but assume
	# that it is needed to get melt fluxes over equivalent time to Q_Mi)
	#M_a = ((Q_Ma * delta_t) / ((rho_old[0]/917.) * lhfi)) 
	M_a = np.maximum(0, (1 / Lfh[0]) * (Q_Ma * delta_t) )
	printv('M_a', M_a)

	# 4.7 volume of ice lost through surface lowering (layer 1)
	# Required as input to Eqn. 4.6.a
	# m^3
	V_a = M_a / rho_old[0] # 'wherein rho is ice density at the beginning of the time period'
	printv('V_a', V_a)

	# 4.6.a Compute new value of rho for uppermost layer
	# kg m-3
	rho[0] = (M[0] - M_i[0] - M_a) / (V_o[0] - V_a)
	
	# 4.6.b Compute new value of rho for L=2-6
	# kg m-3
	rho[1:] = (M[1:] - M_i[1:]) / V_o[1:]
	printv('rho', rho)
	
	# Volume lost from this layer at this point in time
	# m^3
	V = V_o - V_a
	printv('V', V)

	# 4.8 / 4.9 / 4.10 Replenishment of each layer volume 
	# This is important, because volume CANNOT change given that layer thicknesses are invariant
	for n in np.arange(0, 6): #don't run on lowest layer . . . 
	
		if n == 5:
			rho_under = 890.
		else:
			rho_under = rho[n+1]

		if M_a > 0:
			# 4.8 Replenishment layer by layer
			# m^3
			M[n] = (rho[n] * (V_o[n] - V_a)) + (rho_under * V_a)
		else:
			# Eqn 4.10 has limiting case when M_a and therefore V_a is equal to zero.
			# Internal melt is occurring in all layers but there is no replenishment allowed for
			# in the simulation unless ablation is occurring at the same time, thus:
			# Eqn 4.11
			# kg
			M[n] = rho[n] * V[n] 

		# Eqn 4.9, simplified as Eqn 4.10
		# Calculate input density for the next time period
		# kg m-3
		rho[n] = (rho[n] * (1 - (V_a / V_o[n]))) + (rho_under * (V_a / V_o[n]))

		# Eqn 4.11 will collapse as further internal melt occurs and density values
		# will eventually become negative, simulating the depletion of the 
		# surface layer through internal melt. Therefore, a condition is 
		# generated where:
		# Eqn 4.12
		if rho[n] < 0:
			print(t, n, rho[n])
			rho[n] = rho_under

			# When K_star[l=1] is positive but Q_Ma is negative, the surface layer 
			# will be replenished from subsequent layers when necessary. Under 
			# these conditions K_star[l=1] is calculated as in Eqn. 4.2 but instead
			# of input being just K_star it becomes K_star+Q_H+L_star+Q_E to 
			# account for energy loss from the surface. This reduces internal melt
			# in the topmost layer of ice but allows internal melt of the other 
			# layers to continue as usual. Crustal collapse is invoked when ice 
			# density at the end of a time period has a density <= 0 and the 
			# surface layer is allowed to be replenished from lower layers,
			# Eqn 4.13
			# kg
			M[n] = (rho[n] * (V_o[n] - 1e5)) + (1e5 * rho_under)

	# printv('M', M)
	# printv('rho', rho)
	rho_store[t] = pd.Series(deepcopy(rho),index=layers_thk)
	M_store[t] = pd.Series(deepcopy(M), index=layers_thk)

rho_out = pd.DataFrame.from_dict(rho_store, orient='index')
M_out = pd.DataFrame.from_dict(M_store, orient='index')


# At the moment, doesn't address liquid refreeze - there are some suggestions
# about this in Schuster's thesis.