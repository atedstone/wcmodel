import xarray as xr
import pandas as pd
import numpy as np
from copy import deepcopy

import mar_raster

import wcmodel

#mar = mar_raster.open_xr('/scratch/MARv3.9.6-15km-ERAI/MARv3.9.6-2016.nc')
mar1 = mar_raster.open_xr('/scratch/MAR_GRo_out/outputs_MARv3.9.6/hindcast_20190331/ICE.2016.04-09.o55.nc')
mar2 = mar_raster.open_xr('/scratch/MARv3.9-20km-ERAI/ICE.2016.01-12.c56.nc')
mar1 = mar1.sel(TIME=slice('2016-06-10', '2016-08-25'))
mar = mar2.sel(TIME=slice('2016-06-10', '2016-08-25'))

#S5
#mar = mar.sel(X=-436682.912962, Y=-343670.630199, method='nearest')
# S6
# mar = mar.sel(X=-407029.228187, Y=-351945.641246, method='nearest')
# mar1 = mar1.sel(X=-407029.228187, Y=-351945.641246, method='nearest')
# S9
mar = mar.sel(X=-358463.511576, Y=-360624.526866, method='nearest')
mar1 = mar1.sel(X=-358463.511576, Y=-360624.526866, method='nearest')

mar_index = pd.date_range(start='2016-06-10 01:00', end='2016-08-26', freq='1H')

K_star_all = (mar.SWDH * mar.ALH).to_pandas().stack()
K_star_all.index = mar_index
K_star_all = K_star_all.resample('5min').ffill()

LWR = (mar.LWDH - mar.LWUH).to_pandas().stack()
LWR.index = mar_index
LWR = LWR.resample('5min').ffill()

SHF = mar.SHFH.to_pandas().stack()
SHF.index = mar_index
SHF = SHF.resample('5min').ffill()

LHF = mar.LHFH.to_pandas().stack()
LHF.index = mar_index
LHF = LHF.resample('5min').ffill()

alb = mar.ALH.to_pandas().stack()
alb.index = mar_index
alb = alb.resample('5min').ffill()

biomass = mar1.ALGPOP.to_pandas().squeeze().resample('5min').ffill()

Tair = mar.TTH.to_pandas().stack()
Tair.index = mar_index
Tair = Tair.resample('5min').ffill()
Tair = Tair.rolling('3h').mean().resample('5min').ffill()

#layers_thk = np.array([1.]* 60)
layers_thk = np.array([0.02, 0.02, 0.04, 0.08, 0.16, 0.16, 0.16, 0.32, 0.32])
layers_thk = layers_thk #/ 100 # Convert to metres

# V_a = np.zeros(nlyrs)
# V = deepcopy(V_o)

# Timestep, seconds
delta_t = 300

# Corresponding density of ice kg m-3 for lhfi
rhoI = 920. 

# Initial density at t0, kg m-3.
rhoMax = 890. 

# Minimum density (once a layer is at 200 kg m-3 then K_star energy is used for surface lowering instead)
rhoMin = 300.


model = wcmodel.wcmodel(layers_thk, delta_t, rhoMax, rhoMin)

model.rho = np.array([300., 300., 300., 354., 563., 766., 844., 874., 887.])
#model.rho = np.array([299.,399.2257,454.3940,546.8239,677.0562,808.2555,858.5605,877.8906,888.433])
model.M = model.rho * model.V_o

rho_store = {}
M_store = {}
zl = np.cumsum(layers_thk)

melt_store = {}
Q_Mi_store = {}

for t in pd.date_range('2016-06-11 00:00', '2016-08-24 12:00', freq='5min'):
#for t in K_star_all.index:

	#print(t)
	a = np.array([4]*len(layers_thk))
	a[0] = 0.0022 * biomass.loc[t] + 5.60 #max coe=50 
	#a[0] = 0.00121 * biomass.loc[t] + 5.783
	rho, M, Mi, Ma, Mc, T_ice, Q_Mi = model.run(K_star_all.loc[t], LWR.loc[t], SHF.loc[t], LHF.loc[t], \
		a)

	rho_store[t] = pd.Series(deepcopy(rho), index=zl)
	M_store[t] = pd.Series(deepcopy(M), index=zl)
	melt_store[t] = dict(Mi=deepcopy(Mi), Ma=deepcopy(Ma), Mc=deepcopy(Mc), T_ice=deepcopy(T_ice))
	Q_Mi_store[t] = pd.Series(deepcopy(Q_Mi), index=zl)

melt = pd.DataFrame.from_dict(melt_store, orient='index')
rho_out = pd.DataFrame.from_dict(rho_store, orient='index')
rhoxr = xr.DataArray(rho_out, dims=['time','depth'])
M_out = pd.DataFrame.from_dict(M_store, orient='index')
QMi_out = pd.DataFrame.from_dict(Q_Mi_store, orient='index')

rhoavg = (rho_out * model.get_lyr_proportions()).sum(axis=1)
