"""
Run WCM model ice-sheet-wide forced offline by MAR outputs

"""

import xarray as xr
import pandas as pd
import numpy as np
from copy import deepcopy

import mar_raster

import wcmodel

date_start = '2017-06-01'
date_end = '2017-08-31'
final_index = '2017-09-01'
mar_fn = '/scratch/MARv3.11.1-NCEP-20km/MARv3.11-20km-daily-NCEP-NCARv1-2017.nc'

modis_fn = '/scratch/MOD10A1.006.MAR_GRo_trim/MOD10A1.*.006.MAR_GRo_trim.nc'


layers_thk = np.array([0.02, 0.02, 0.04, 0.08, 0.16, 0.16, 0.16, 0.32, 0.32])

# Timestep, seconds
delta_t = 300

# Corresponding density of ice kg m-3 for lhfi
rhoI = 920. 

# Initial density at t0, kg m-3.
rhoMax = 890. 

# Minimum density (once a layer is at rhoMin then K_star energy is used for surface lowering instead)
rhoMin = 300.


################################################################################

mar = mar_raster.open_xr(mar_fn)
mar = mar.sel(TIME=slice(date_start, date_end))

avg_grid = np.zeros((len(mar.Y),len(mar.X)))
store = {}

n = 1
i = 0
j = 0
for x in mar.X:
	for y in mar.Y:
		if mar.MSK.sel(X=x, Y=y) > 99 and mar.SH.sel(X=x,Y=y) < 1800:

			print(n)
			n += 1

			mar_px = mar.sel(X=x,Y=y)

			# Determine start date as date of snow clearing	
			wc_start_date = mar_px.SHSN2.where(mar_px.SHSN2 < 0.01).dropna(dim='TIME')
			if len(wc_start_date) > 0:
				wc_start_date = wc_start_date.isel(TIME=0).TIME.values
			else:
				continue
			
#			if start_date > np.datetime64('2016-07-25'):
#				continue

			mar_index = pd.date_range(start=start_date+' 01:00', end=final_index, freq='1H')
			K_star_all = (mar_px.SWDH * mar_px.ALH).to_pandas().stack()
			K_star_all.index = mar_index
			K_star_all = K_star_all.resample('5min').ffill()

			LWR = (mar_px.LWDH - mar_px.LWUH).to_pandas().stack()
			LWR.index = mar_index
			LWR = LWR.resample('5min').ffill()

			SHF = mar_px.SHFH.to_pandas().stack()
			SHF.index = mar_index
			SHF = SHF.resample('5min').ffill()

			LHF = mar_px.LHFH.to_pandas().stack()
			LHF.index = mar_index
			LHF = LHF.resample('5min').ffill()

			biomass = mar_px.ALGPOP.to_pandas().squeeze().resample('5min').ffill()
			
			rho_store = {}
			zl = np.cumsum(layers_thk)
			
			model = wcmodel.wcmodel(layers_thk, delta_t, rhoMax, rhoMin)
			#model.rho = np.array([500, 500, 600, 700, 890, 890, 890, 890, 890])
		
			for t in pd.date_range(start_date,date_end, freq='5min'):
				
				a = np.array([2.]*len(layers_thk))
				a[0] = 0.0022 * biomass.loc[t] + 5.60 #max coe=50 

				rho, M, Mi, Ma, Mc, T_ice, Q_Mi = model.run(K_star_all.loc[t], 
					LWR.loc[t], 
					SHF.loc[t], 
					LHF.loc[t], 
					a)

				rho_store[t] = pd.Series(deepcopy(rho), index=zl)

			rho_out = pd.DataFrame.from_dict(rho_store, orient='index')
			rhoavg = (rho_out * model.get_lyr_proportions()).sum(axis=1)
			rhoavgavg = rhoavg.mean()
			avg_grid[j,i] = rhoavgavg
			store[i,j] = rho_out

		
		j += 1
	j = 0
	i += 1


avg_xr = xr.DataArray(avg_grid, dims=('Y','X'), coords={'Y':mar.Y, 'X':mar.X})
figure(),avg_xr.plot(vmin=600, vmax=890)
plot(locs.x,locs.y, 'x')


# Generate daily rho mean per pixel...
store_3d = np.zeros((92,len(mar.Y),len(mar.X)))
all_rho = np.zeros((92,9,len(mar.Y),len(mar.X)))

for px, py in store:
	ts = (store[px,py] * model.get_lyr_proportions()).sum(axis=1)
	#store_3d[:,py,px] = ts.reindex(pd.DatetimeIndex(start='2016-06-01', end='2016-08-31', freq='D'))
	all_rho[:,:,py,px] = store[px,py].reindex(pd.DatetimeIndex(start=date_start, end=date_end, freq='D'))

rho_xr = xr.DataArray(store_3d, dims=('TIME', 'Y', 'X'), 
	coords={'TIME':pd.DatetimeIndex(start=date_start, end=date_end, freq='D'),
		'Y':mar.Y, 'X':mar.X})

all_rho_xr = xr.DataArray(all_rho, dims=('TIME', 'Z', 'Y', 'X'), 
	coords={'TIME':pd.DatetimeIndex(start=date_start, end=date_end, freq='D'),
		'Z':np.arange(1,10), 'Y':mar.Y, 'X':mar.X})







################################################################################

### Now do some comedy back of enevlope checking aginst albedo !!


modAL = xr.open_mfdataset(modis_fn, concat_dim='TIME')
modAL['Y'] = mar.Y  
modAL['X'] = mar.X  

msk_complete = mar_raster.gris_mask(ds_fn=mar_fn)
msk = msk_complete.where(msk_complete > 0.5)

mask_erode = ndimage.binary_erosion(np.where(msk.squeeze() > 0.9,1,0), 
	iterations=1)
mask_erode = np.where(mask_erode == 1, 1, np.nan)
msk_erode = xr.DataArray(mask_erode, dims=('Y','X'), 
	coords={'X':msk.X, 'Y':msk.Y})
msk_erode.name = msk.name



after_snow = mar.SHSN2.where(mar_px.SHSN2 < 0.01).sel(SECTOR1_1=1).resample(TIME='1D').first()


# This is static 14 day windows, NOT rolling
modis_here = modAL.resample(TIME='1D').asfreq()
modis_here = modis_here.sel(TIME=slice(date_start,date_end))
modis14pd = (modis_here.Snow_Albedo_Daily_Tile \
	.where(modis_here.Snow_Albedo_Daily_Tile < 100) \
	.where(msk_erode.notnull()) \
	.where(after_snow.shift(TIME=5).notnull()) \
	.resample(TIME='14D').mean(dim='TIME') * 0.01).to_series()
modis14pd.name = 'MODIS'

rhomsked = rho_xr.where(modis_here.Snow_Albedo_Daily_Tile < 100).where(msk_erode.notnull()).where(after_snow.shift(TIME=5).notnull())
rhomsked = rhomsked.where(rhomsked > 400)

rhomsked_14 = rhomsked.resample(TIME='14D').mean(dim='TIME').to_series()

biomass_14 = mar.ALGPOP.sel(TIME=slice(date_start,date_end)).resample(TIME='1D').first()
asdf = pd.concat([modis14pd, rhomsked_14, biomass_14.to_series()], axis=1)

X = df[['k']]
X = sm.add_constant(X)

#dz2
y = df['alb']
model = sm.OLS(y, X, missing='drop') 
fit = model.fit()



rho_jja = rho_xr.where(after_snow.notnull()).mean(dim='TIME')


############################### monday afternoon, post tea break brain fail

# Also tried running for latitudes < 70oN, 2016 only...still apparently no luck.

# Could be a snow vs ice masking issue in the MODIS data. However, I am at least culling for all MODIS values < 0.55. NO I'M NOT


alb_jja = modis_here.Snow_Albedo_Daily_Tile \
	.where(modis_here.Snow_Albedo_Daily_Tile < 60) \
	.where(mar.LAT < 70) \
	.where(mar.MSK > 90) \
	.where(msk_erode == 1) \
	.where(after_snow.notnull()).mean(dim='TIME')


rho_jja = rho_xr.where(after_snow.notnull()) \
	.where(mar.LAT < 70) \
	.where(mar.MSK > 90) \
	.where(msk_erode == 1) \
	.mean(dim='TIME')

gc = xr.open_dataset('/scratch/MAR_GRo_out/outputs_MARv3.9.6/hindcast_20190331/growthcurves_full.nc')
k = gc.K.sel(TIME='2016') \
	.where(mar.LAT < 70) \
	.where(mar.MSK > 90) \
	.where(msk_erode == 1) \
	.squeeze()

df = pd.concat([
	alb_jja.to_series(),
	rho_jja.to_series(),
	k.to_series()], axis=1)
df.columns = ['alb','rho','k']

""" 15 Jul 17:02 WITHOUT msk_erode:
In [613]: df.corr()                                                                                 
Out[613]: 
          alb       rho         k
alb  1.000000 -0.006937 -0.535225
rho -0.006937  1.000000  0.092774
k   -0.535225  0.092774  1.000000

with msk_erode:
In [616]: df.corr()                                                                                 
Out[616]: 
          alb       rho         k
alb  1.000000 -0.078127 -0.572729
rho -0.078127  1.000000  0.211550
k   -0.572729  0.211550  1.000000

Next steps: if just validating with MODIS albedo for latitudes < 70, is
existing MAR solution sufficient?
Think about further masking options
K coming out OK but need to (re-)consider the role of the WC model in the context of what I'm tryin to do
Do we go south-west only? That's almost what a 70oN latitude mask does to the validation anyway.

Validate MAR snow line position?? E.g. by thresholding MOD10A1?
"""



dfm = df[df.rho > 400]
X = dfm[['rho']]
X = sm.add_constant(X)

#dz2
y = dfm['alb']
model = sm.OLS(y, X, missing='drop') 
fit = model.fit()
#
## What about model match MAR-AL vs MODIS for south-west only? (scatter plot)


mar_alb_jja = mar_alb \
	.where(modis_here.Snow_Albedo_Daily_Tile < 60) \
	.where(mar.LAT < 70) \
	.where(mar.MSK > 90) \
	.where(msk_erode == 1) \
	.where(after_snow.notnull()).mean(dim='TIME')

mar_alb_daily = mar_alb \
	.where(modis_here.Snow_Albedo_Daily_Tile < 60) \
	.where(mar.LAT < 70) \
	.where(mar.MSK > 90) \
	.where(msk_erode == 1) \
	.where(after_snow.notnull()).resample(TIME='14D').mean(dim='TIME')

alb_daily = modis_here.Snow_Albedo_Daily_Tile \
	.where(modis_here.Snow_Albedo_Daily_Tile < 60) \
	.where(mar.LAT < 70) \
	.where(mar.MSK > 90) \
	.where(msk_erode == 1) \
	.where(after_snow.notnull()).resample(TIME='14D').mean(dim='TIME')

dailydf = pd.concat([mar_alb_daily.to_series(),alb_daily.to_series()],axis=1)