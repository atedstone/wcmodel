"""
This is a non-working attempt at implementing MAR's snow sub-surface energy
budget scheme for ice.

July 2019
"""

#x = (Q*(1-exp(-0.5))) / 336000


# Latent heat of fusion ice J/kg
Lf = 333700

# Snow heat capacity J/kg/K
Cn = 2105. 

# Water heat capacity J/kg/K
Cw = 4186.

# Snow melting temperature K
TfSnow = 273.15

# Layer thickness
dz = 0.01

# Layer density
rho = 300.

# Timestep seconds
dt = 300

# Ice temperature
Tsis = -5 + 273.15

wer = 0
store = {}
drr = 0
dzMelt = 0

for ix,row in met_data.iterrows():
	
	EExcsv = Cw * (row.Tsurf_calc) * dt

	dTSnow = Tsis - TfSnow
	EExcsv = rho * Cn * dTSnow * dz
	Tsis = TfSnow

	drr = drr + rho * eta * dz / dt

	# Melt
	EnMelt = np.maximum(0, EExcsv)
	#drr = drr + rho * dzMelt / dt

	# Freeze
	LayrOK = 1
	rdzsno = rho * dz
	EnFrez1 = np.minimum(0, EExcsv)
	drrNEW = np.maximum(0, drr - WaFrez/dt)
	WaFrez = -(EnFrez1 * LayrOK / Lf) * dt
	EExcsv = EExcsv + WaFrez * Lf
	EnFrez2 = np.minimum(0, EExcsv) * LayrOK
	rdznew = WaFrez + rdzsno
	rho = rdznew / max(0, dz)
	Tsis = TfSnow + (EnFrez2 / (Cn * np.maximum(0, rdznew)))
	EExcsv = EExcsv - EnFrez2
	wer = WaFrez + wer

	store[ix] = dict(Tsis=deepcopy(Tsis), wer=deepcopy(wer), 
		EnFrez1=deepcopy(EnFrez1), EnFrez2=EnFrez2, energy1=energy1, WaFrez=deepcopy(WaFrez))

storepd = pd.DataFrame.from_dict(store, orient='index')



Q = K_star_all + LWR + SHF + LHF
rho = 700
vol = 0.5
T_ice = 0.
store = {}
dt = 300
for ix, row in Q.iteritems():

	energy = row * dt
	J = Cn * (rho * vol)
	T_change = energy / J
	T_ice = T_ice + T_change
	T_ice = np.minimum(T_ice, 0)

	store[ix] = dict(J=J, T_ice=T_ice)

storepd = pd.DataFrame.from_dict(store, orient='index')

