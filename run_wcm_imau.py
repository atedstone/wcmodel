import xarray as xr
import pandas as pd
import numpy as np
from copy import deepcopy
import datetime as dt


from wcmodel import wcmodel


site_id = 'S6'
year = 2016
ts_start = dt.datetime(2016,6,20)
ts_end = dt.datetime(2016,8,30)
def date_parser(year, day, hour):
	year = int(float(year))
	day = int(float(day))
	hour = int(float(hour))
	return dt.datetime.strptime('%i-%i-%i' %(year,day,hour), '%Y-%j-%H')

if year in np.arange(2012, 2017) and site_id == 'S6':
	fn = '/scratch/metdata/S6_SEB_2012_2016_Tedstone.txt'
elif year in np.arange(2010, 2012) and site_id == 'S6':
	fn = '/scratch/metdata/S6_SEB_2010_2011_Tedstone.txt'
elif year in np.arange(2008, 2011) and site_id == 'S6':
	fn = '/scratch/metdata/S6_SEB_2008_2010_Tedstone.txt'
elif year in np.arange(2003, 2008) and site_id == 'S6':
	fn = '/scratch/metdata/S6_SEB_2003_2007_Tedstone.txt'

elif site_id == 'S5':
	fn = '/scratch/metdata/S5_SEB_2003_2016_Tedstone.txt'
elif site_id == 'S9':
	fn = '/scratch/metdata/S9_SEB_2003_2016_Tedstone.txt'
elif site_id == 'S10':
	fn = '/scratch/metdata/S10_SEB_2003_2016_Tedstone.txt'


met_data_full = pd.read_csv(fn,
	delim_whitespace=True, parse_dates={'datetime':[0,1,2]}, 
	date_parser=date_parser, index_col='datetime', 
	keep_date_col=True)

met_data = met_data_full[ts_start:ts_end]

met_data = met_data.drop(labels=('time'), axis=1)

biomass = pd.read_csv('/home/at15963/scripts/wcmodel/algpop_%s_2016.csv' %site_id,
	index_col=0, parse_dates=True, header=None).squeeze().resample('5min').ffill()

K_star_all = met_data.SWnet_corr
K_star_all = K_star_all.resample('5min').ffill()

LWR = met_data.Lwnet_corr.resample('5min').ffill()
SHF = met_data.Hsen.resample('5min').ffill()
LHF = met_data.Hlat.resample('5min').ffill()

layers_thk = np.array([1.]* 60)
layers_thk = np.array([0.01, 0.01, 0.02, 0.04, 0.08, 0.16, 0.32])
layers_thk = layers_thk #/ 100 # Convert to metres

# V_a = np.zeros(nlyrs)
# V = deepcopy(V_o)

# Timestep, seconds
delta_t = 300

# Corresponding density of ice kg m-3 for lhfi
rhoI = 920. 

# Initial density at t0, kg m-3.
rhoMax = 890. 

# Minimum density (once a layer is at 300 kg m-3 then K_star energy is used for surface lowering instead)
rhoMin = 300.


model = wcmodel(layers_thk, delta_t, rhoMax, rhoMin)

# Decay term in Beer's Law, after Geiger 1965, their units are cm
a_const = 0.06 * 100 # convert to metres-1

rho_store = {}
M_store = {}
melt_store = {}
zl = np.cumsum(layers_thk)


for t in K_star_all.index:

	a = np.array([6]*(60))
	#a = -0.03 * model.rho +  36.96
	a[0] = 0.0022 * biomass.loc[t] + 5.60
	rho, M, Mi, Ma, Mc, T_ice, Q_Mi = model.run(K_star_all.loc[t], LWR.loc[t], SHF.loc[t], LHF.loc[t], \
		a)

	rho_store[t] = pd.Series(deepcopy(rho), index=zl)
	M_store[t] = pd.Series(deepcopy(M), index=zl)
	melt_store[t] = dict(Mi=deepcopy(Mi), Ma=deepcopy(Ma), Mc=deepcopy(Mc))

rho_out = pd.DataFrame.from_dict(rho_store, orient='index')
melt = pd.DataFrame.from_dict(melt_store, orient='index')
rhoxr = xr.DataArray(rho_out, dims=['time','depth'])
M_out = pd.DataFrame.from_dict(M_store, orient='index')

rhoavg = (rho_out * model.get_lyr_proportions()).sum(axis=1)

"""
To do - add function to calculate a?
To do - add validation of model inputs to wcmodel interface
"""



"""
		print(self.M[0])
		print(M_i[0])
		print(M_a)
		print(M_c)
		print(self.V_o[0])
		print(V_a)
		print(V_c)
"""