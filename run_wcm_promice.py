import xarray as xr
import pandas as pd
import numpy as np
from copy import deepcopy


import wcmodel


promice_full = xr.open_dataset('/scratch/PROMICE/KAN_M_hour_v03.nc')
promice = promice_full.sel(time=slice('2016-07-01 12:00', '2016-08-25')).resample(time='5min').ffill()
#promice = promice_full.sel(time=slice('2016-07-01 12:00', '2016-07-02 12:00')).resample(time='5min').ffill()

# Eventually this would ideally be coupled to an albedo model which means
# that energy fluxes will then need to be computed at every step.
# At the moment we're using observed albedo (c/f K_star)
K_star_all = promice.shortwave_radiation_down_cor - promice.shortwave_radiation_up_cor
K_star_all = K_star_all.to_series().interpolate()

LWR = (promice.longwave_radiation_down - promice.longwave_radiation_up).to_series().interpolate()
SHF = promice.sensible_heat_flux.to_series().interpolate()
LHF = promice.latent_heat_flux.to_series().interpolate()

layers_thk = np.array([0.02, 0.02, 0.04, 0.08, 0.16, 0.16, 0.16, 0.32, 0.32])

# Timestep, seconds
delta_t = 300

# Corresponding density of ice kg m-3 for lhfi
rhoI = 920. 

# Initial density at t0, kg m-3.
rhoMax = 890. 

# Minimum density (once a layer is at rhoMin then K_star energy is used for surface lowering instead)
rhoMin = 300.


model = wcmodel.wcmodel(layers_thk, delta_t, rhoMax, rhoMin)

# Decay term in Beer's Law, after Geiger 1965, their units are cm
a_const = 0.06 * 100 # convert to metres-1

rho_store = {}
M_store = {}
zl = np.cumsum(layers_thk)
melt_store = {}
Q_Mi_store = {}

alb = promice.albedo_theta.to_pandas().rolling('24h').mean().resample('5min').ffill()

for t in K_star_all.index:

	a = np.array([10]*len(layers_thk))
	#a = -0.03 * model.rho +  36.96
	#a[0] = 0.0022 * biomass.loc[t] + 5.60 #max coe=30
	#a[0] = (-0.07 * biomass.loc[t] + 75.65) * 5
	#a[0] = np.minimum(np.maximum(4, -86.6 * alb.loc[t] + 56), 30)
	rho, M, Mi, Ma, Mc, T_ice, Q_Mi = model.run(K_star_all.loc[t], LWR.loc[t], SHF.loc[t], LHF.loc[t], \
		a)

	rho_store[t] = pd.Series(deepcopy(rho), index=zl)
	M_store[t] = pd.Series(deepcopy(M), index=zl)
	melt_store[t] = dict(Mi=deepcopy(Mi), Ma=deepcopy(Ma), Mc=deepcopy(Mc), T_ice=deepcopy(T_ice))
	Q_Mi_store[t] = pd.Series(deepcopy(Q_Mi), index=zl)

melt = pd.DataFrame.from_dict(melt_store, orient='index')
rho_out = pd.DataFrame.from_dict(rho_store, orient='index')
rhoxr = xr.DataArray(rho_out, dims=['time','depth'])
M_out = pd.DataFrame.from_dict(M_store, orient='index')
QMi_out = pd.DataFrame.from_dict(Q_Mi_store, orient='index')

rhoavg = (rho_out * model.get_lyr_proportions()).sum(axis=1)


## Estimate surface lowering?
