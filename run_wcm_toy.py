import xarray as xr
import pandas as pd
import numpy as np
from copy import deepcopy


import wcmodel



K_star_all = 350.
LWR = 60.
SHF = -10.
LHF = -1.

layers_thk = np.array([0.02, 0.02, 0.04, 0.08, 0.16, 0.16, 0.16, 0.32, 0.32])
layers_thk = np.array([0.1, 0.2, 0.2, 0.4, 0.8])
# MAR upper 1 m
layers_thk = np.array([0.05, 0.05, 0.1, 0.1, 0.1, 0.1, 0.15, 0.15, 0.2])

a = np.array([2]*len(layers_thk))
a[0:2] = 4.

# Timestep, seconds
delta_t = 300

# Corresponding density of ice kg m-3 for lhfi
rhoI = 920. 

# Initial density at t0, kg m-3.
rhoMax = 890. 

# Minimum density (once a layer is at 200 kg m-3 then K_star energy is used for surface lowering instead)
rhoMin = 300.


model = wcmodel.wcmodel(layers_thk, delta_t, rhoMax, rhoMin)

# model.rho = np.array([300., 300., 300., 354.,
#        563.89295117, 766.74870075, 844.90764575, 874.53190302,
#        887.89663846])
# model.M = model.rho * model.V_o


rho_store = {}
M_store = {}
melt_store = {}
zl = np.cumsum(layers_thk)

times = np.arange(0, 2250, 1)
for t in times:

	rho, M, Mi, Ma, Mc, T_ice, Q_Mi = model.run(K_star_all, LWR, SHF, LHF, \
		a)

	rho_store[t] = pd.Series(deepcopy(rho), index=zl)
	M_store[t] = pd.Series(M, index=zl)
	melt_store[t] = dict(Mi=deepcopy(Mi), Ma=deepcopy(Ma), Mc=deepcopy(Mc))

rho_out = pd.DataFrame.from_dict(rho_store, orient='index')
rhoxr = xr.DataArray(rho_out, dims=['time','depth'])
M_out = pd.DataFrame.from_dict(M_store, orient='index')
melt = pd.DataFrame.from_dict(melt_store, orient='index')

rhoavg = (rho_out * model.get_lyr_proportions()).sum(axis=1)
